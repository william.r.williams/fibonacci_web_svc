Fibonacci Web Service Demo

Installation for For Ubuntu 14 or 16 (you will need sudo access)

1.) Create a project directory and cd into that directory e.g. `mkdir ~/fib && cd ~/fib`

2.) Retrieve the project from gitlab.com:

    git clone https://gitlab.com/william.r.williams/fibonacci_web_svc.git .

3.) Execute this script that was just downloaded, you will be prompted for sudo:

    ./setup_nginx_uwsgi_fibonacci_web_svc.sh

4.) Run tests to make sure everything is ready:

    ./fibonacci_web_svc_tester


If the tests above succeed you can then execute the app like this:

    curl -i  http://127.0.0.1/wrw-fibonacci/<whole_positive_number>    

or open a browser with the URL:

    http://127.0.0.1/wrw-fibonacci/<whole_positive_number>    



-Bill W.
Nov2017
