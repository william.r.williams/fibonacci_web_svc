#!/bin/bash
####################################################################################################
#   setup_nginx_uwsgi_fibonacci_web_svc.sh
#   Configure an Ubuntu 14 or 16 system to support the Fibonacci Web Service Demo project.
#   Note that the following software does get installed: python-pip, python-dev, nginx,
#   virtualenv, uwsgi, & flask
#   Both the uwsgi and nginx services get configured to start upon boot.
#   -William Williams 07Nov2017
#   william.r.williams@gmail.com                         
####################################################################################################

# Is this one of the correct platforms?
! python -mplatform | grep -qE "Ubuntu-14|Ubuntu-16" && echo "This is not an Ubuntu 14 or 16 system"  && exit 1

## Set up environment

# Uncomment & edit below if a proxy is required
#PIPARGS="--proxy http://<some_proxy>:<port>"
PROJDIR="$(pwd)"
VENVDIR="$(basename ${PROJDIR})-env"

sudo apt-get -y install python-pip python-dev nginx

sudo pip ${PIPARGS} install virtualenv

virtualenv "${VENVDIR}"

source "${VENVDIR}"/bin/activate

pip ${PIPARGS} install uwsgi flask

deactivate

## Edit and move config files into place - uwsgi first

if python -mplatform | grep -q "Ubuntu-14"; then
  # Configure for Upstart
  sed -e "s/^setuid.*$/setuid ${USER}/" \
  -e "s/^env PATH=.*$/env PATH=$(sed 's:/:\\/:g'  <<<"${PROJDIR}/${VENVDIR}/bin/")/" \
  -e "s/^chdir.*$/chdir $(sed 's:/:\\/:g'  <<<"${PROJDIR}")/" \
  -i fibonacci_web_svc_uwsgi.conf
  sudo mv fibonacci_web_svc_uwsgi.conf /etc/init/
  sudo start fibonacci_web_svc_uwsgi
elif python -mplatform | grep -q "Ubuntu-16"; then
  # Configure for Systemd
  sed -e "s/^User=.*$/User=${USER}/" \
  -e "s/^Environment=.*$/Environment=\"PATH=$(sed 's:/:\\/:g'  <<<"${PROJDIR}/${VENVDIR}/bin/")\"/" \
  -e "s/^WorkingDirectory=.*$/WorkingDirectory=$(sed 's:/:\\/:g'  <<<"${PROJDIR}")/" \
  -e "s/^ExecStart=.*$/ExecStart=\"$(sed 's:/:\\/:g'  <<<"${PROJDIR}/${VENVDIR}/bin/uwsgi")\" --ini fibonacci_web_svc.ini/" \
  -i fibonacci_web_svc_uwsgi.service
  sudo mv fibonacci_web_svc_uwsgi.service /etc/systemd/system/
  sudo systemctl enable fibonacci_web_svc_uwsgi.service && sudo systemctl start fibonacci_web_svc_uwsgi.service
fi

# now nginx
sed  -e "s/uwsgi_pass unix:.*$/uwsgi_pass unix:$(sed 's:/:\\/:g'  <<<"${PROJDIR}/fib.sock;")/" \
  -i fibonacci_web_svc

sudo mv fibonacci_web_svc /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/fibonacci_web_svc /etc/nginx/sites-enabled
sudo service nginx restart
