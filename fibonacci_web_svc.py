####################################################################################################
#   fibonacci_web_svc.py
#   This is the fibonacci web service demo python flask application.
#   -William Williams 06Nov2017
#   william.r.williams@gmail.com                         
####################################################################################################

from flask import Flask, jsonify, abort, make_response

app = Flask(__name__)

@app.errorhandler(404)
def not_found(error):
  return make_response(jsonify({'error': 'Value submitted must be a positive whole number'}), 404)


@app.route('/wrw-fibonacci/<int:user_input>', methods=['GET'])
def get_fib_seq(user_input):

  # User input error checking here
  if type(user_input) != int or user_input < 0:
    abort(404)

  given_num = user_input
  firstFibNum = 0
  secondFibNum = 1
  thirdFibNum = 0
  fib_seq = [firstFibNum, secondFibNum]

  # Generate the fibonacci sequence as a list here
  while thirdFibNum < given_num:
    thirdFibNum = firstFibNum + secondFibNum
    fib_seq.append(thirdFibNum)
    firstFibNum = secondFibNum
    secondFibNum = thirdFibNum

  # Last item in the list always exceeds the given number
  if fib_seq[-1] >= given_num:
    fib_seq.pop()

  return jsonify({'SEQ': fib_seq})


if __name__ == '__main__':
    app.run(debug=True)

